import Foundation
import UIKit

enum UserInfo {
    static var username: String? = "username"
    static var image: UIImage? = UIImage(named: "avatar")
}
