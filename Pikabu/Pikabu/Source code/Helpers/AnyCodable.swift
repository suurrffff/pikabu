import Foundation

typealias AnyCodable = Any & Codable
